# hdf5-workshop

Contents for a Mini-Workshop introducing basics and benefits of the hdf5 file format.  

Further documentation regarding the code examples are provided via step-by-step comments, start with matlab examples!  
* matlab examples are provided in [h5basics.m](https://git.rwth-aachen.de/nils.preuss/hdf5-workshop/-/blob/master/h5basics.m)  
* python examples are provided in [h5basics.py](https://git.rwth-aachen.de/nils.preuss/hdf5-workshop/-/blob/master/h5basics.py)  

This Repository also provides various examples of used or generated data files:
* Usage examples of different applications and stakeholders can be found in this [subdirectory](https://git.rwth-aachen.de/nils.preuss/hdf5-workshop/-/tree/master/examplefiles_hdfgroup)  
* Examples of different text-based markup formats for data or metadata can be found in this [subdirectory](https://git.rwth-aachen.de/nils.preuss/hdf5-workshop/-/tree/master/examplefiles_markup)  
* The h5-file resulting from the code examples can be found [here](https://git.rwth-aachen.de/nils.preuss/hdf5-workshop/-/tree/master/examplefiles_FST)  
* The metadata model employed by FST makes use of labeling different elements via attributes of an arbitrary data structure for later cataloguing. An example file is provided [here](https://git.rwth-aachen.de/nils.preuss/hdf5-workshop/-/tree/master/examplefiles_FST)  

A recording of the presentation and hands-on examples of the Workshop can be found [here](https://tu-darmstadt.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=144884dc-c138-4235-a932-ac9301384c93) (the discussion part was not recorded)  
The following subsections provide an introduction (transcribed excerpt of the used [ppt slides](https://git.rwth-aachen.de/nils.preuss/hdf5-workshop/-/blob/master/presentation_201110_KnowHowClusterPumpendiagnose_HDF5WS_preuss.pdf) in german):

## Ein vielseitiges Dateiformat für heterogene und komplexe Daten?
* Vielzahl von verwendeten Formaten je nach Anwendung und Programmiersprache
* Formate teilweise ungeeignet für Primärdaten oder reichhaltige Metadaten

Bsp: pdf, txt, xls, xml, tdms, mat, bin, hdf5  
-> wesentliches Hindernis für Entwicklung, Austausch und Nachnutzung von Daten und Implementierungen

Anforderungen:
* flexible Datenstruktur
* selbstbeschreibend
* interoperabel
* unabhängig
* simple Schnittstellen

-> HDF5 als geeignete Grundlage für formale Datenqualität
XML, JSON vergleichbar für Metadaten, nicht aber Rohdaten!

## Learning Outcomes des Workshops

Am unteren Ende des Anwendungsspektrums:  
HDF5 kann ohne hohen Aufwand für Setup und Einarbeitung eingesetzt werden, unabhängig von Programmiersprache und Anwendungsfall.
-> großes Plus an Interoperabilität mit geringem Aufwand bzw. Invest


Am oberen Ende des Anwendungsspektrums:  
HDF5 bietet Features um Ansprüchen hinsichtlich Performance und Interoperabilität auch in Edgecases gerecht zu werden.(HPC, Bildgebende Verfahren, große Anzahl Mikro-Datensätze)
Zukunftssichere, vielseitige Grundlage

## Adressaten

* Wissenschaftliches Personal, F&E
* Versuchs-, Berechnungsingenieur*Innen
* Beteiligte am Datenaustausch Forschungsstellen -> Industrie

Demo Beispiele + Doku werden zur Verfügung gestellt
in diesem repo -> https://git.rwth-aachen.de/nils.preuss/hdf5-workshop

## Contents 

Einführung

1. allg. Vorteile und typische Anforderungen bzgl. Qualität der Forschungsdaten
2. zur Interoperabilität und Metadaten

Demo (Matlab, Python, HDFView)

1. Basics: Nutzung als Datenstruktur für Primärdaten, Metadaten
2. Nutzung als „Trägerformat“


