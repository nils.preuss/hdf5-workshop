%% intro
% define file path & name
fpath = "./example.h5";

% examples for datasets of relevant base data types

var_pi = pi;

p = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89];
Q = [18, 18, 18, 18.1, 18, 17.9, 17.8, 17.7, 17.6, 17, 16, 8];
T = 293.15 + rand(1, 12);

textstr = "hello world";

textarray = ["ULB", "<3", "HDF5"];

% create hdf5 file and dataset > write data to hdf5 file / dataset
h5create(fpath, "/pi", size(var_pi), "Datatype", class(var_pi))
h5write(fpath, "/pi", var_pi)

%% group & dataset examples
% regarding terminology c.f. ppt sketch > write some groups and datasets
% users can refer to Elements comparable to URLs
% c.f. visualization of structure and datasets in HDFView
h5create(fpath, "/data/p", size(p), "Datatype", class(p))
h5write(fpath, "/data/p", p)

h5create(fpath, "/data/Q", size(Q), "Datatype", class(Q))
h5write(fpath, "/data/Q", Q)

h5create(fpath, "/data/T", size(T), "Datatype", class(T))
h5write(fpath, "/data/T", T)

h5create(fpath, "/text/string", size(textstr), "Datatype", class(textstr))
h5write(fpath, "/text/string", textstr)

h5create(fpath, "/text/array", size(textarray), "Datatype", class(textarray))
h5write(fpath, "/text/array", textarray)

%% random access
% HDF5 provides random Acces to contained Elements!
% c.f.: read two Arrays and plot them
% notice no specification of datatype or dimensions is needed!
plot(h5read(fpath, "/data/p"), h5read(fpath, "/data/Q"));

%% Metadata
% HDF5 is a self-describing format, technical metadata is built in
% to expand this, write some custom attributes for datasets
h5writeatt(fpath, "/pi", "description", "this dataset has the value pi")
h5writeatt(fpath, "/pi", "timestamp", "yyyy-MM-ddThh:mm:ss")

h5writeatt(fpath, "/data/T", "samplerate", 42069)

% show explorative interaction with file contents
h5info(fpath, "/")  % > 3 Groups
h5info(fpath, "/").Groups.Name  % > /data, /image, /text
h5info(fpath, "/data")  % > 3 Datasets

%% Attribute Metadata example: Parsing
% example usage of this feature: lets assume a disjunct program that uses
% the stored data > interoperability between different programs 
% find all datasets with attribute "samplerate" > aggregate > write
dsets = [h5info(fpath, "/data").Datasets];
for ii = 1:length(dsets)
    dset = dsets(ii);
    if ~isempty(dset.Attributes) ...
        && any({dset.Attributes.Name} == "samplerate")
        
        dsetname = strcat("/data/", dset.Name);
        values = h5read(fpath, dsetname);
        
        values_mean = mean(values);
        dsetname_mean = strcat(dsetname, "_mean");
        h5create(fpath, dsetname_mean, size(values_mean), ...
                "Datatype", class(values_mean))
        h5write(fpath, dsetname_mean, values_mean)
        h5writeatt(fpath, dsetname_mean, "source", dsetname)
        
        values_std = std(values);
        dsetname_std = strcat(dsetname, "_std");
        h5create(fpath, dsetname_std, size(values_std), ...
                "Datatype", class(values_std))
        h5write(fpath, dsetname_std, values_std)
        h5writeatt(fpath, dsetname_std, "source", dsetname)
    end
end

%% Attribute Metadata example: Interpreting Data
% c.f.: this is ofc the "working principle" of HDFView:
% technical metadata is used to traverse the structure and view data
% predefined attributes are used to identify arrays that hold image data
ImageData = permute(imread("./TUDAathene.png"), [3 2 1]);
ImageURL = "/image/TUDAathene";
h5create(fpath, ImageURL, size(ImageData), "Datatype", class(ImageData))
h5write(fpath, ImageURL, ImageData)
h5writeatt(fpath, ImageURL, "CLASS", 'IMAGE', ...
            "TextEncoding", "system")
h5writeatt(fpath, ImageURL, "IMAGE_SUBCLASS", 'IMAGE_TRUECOLOR', ...
            "TextEncoding", "system")
h5writeatt(fpath, ImageURL, "IMAGE_VERSION", '1.2', ...
            "TextEncoding", "system")
h5writeatt(fpath, ImageURL, "INTERLACE_MODE", 'INTERLACE_PIXEL', ...
            "TextEncoding", "system")

%% Interoperability across Languages
% go to python [...]

% read this metadata back in matlab
h5info(fpath, "/")
h5info(fpath, "/").Groups.Name
h5info(fpath, "/best_oil").Attributes.Name

%% "HDF5 as Carrier" Example
% go to python [...]

% retrieve json data
fpath = "./example.h5";
best_oil = jsondecode(h5read(fpath, "/best_oil/json"));

% Wrapping lots of file contents in HDF5 leverages its Performance as well
% as self-descriptive and interoperability features
% c.f. copying large numbers of images or logfiles

% HDF5 enables combinations of heterogeneous, "big" primary data
% and complex metadata, c.f. examplefiles_hdfgroup/index.h5

% summary: show hdfgroup examples
% eye-catching example: .mat files are implemented on top of HDF5 using
% these features
% c.f. workspace as .mat file in HDFView

save("./workspace.mat", "-v7.3")