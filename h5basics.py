import h5py
import numpy as np
import json

# HDF5 does not make assumptions about language specific data structures
# > interoperability between programs in different languages
# c.f.: read the stored data step by step
f = h5py.File("./example.h5")

print(list(f.keys()))

for item in f["/data"]:
    print(item)

values = f["/data/Q"][:]

f.close()

# user can use structure and attributes to represent auxillary information
# metadata
# example usage: create a dict that holds properties of a hydraulic fluid
# write a hdf5 group that represents a hydraulic fluid, use
# attributes to represent its properties
best_oil = {
        "vendor": "Ölprinz Inc.",
        "density": 420,
        "viscosity": 69
}

f = h5py.File("./example.h5", "r+")

grp = f.require_group("best_oil")

for key, value in best_oil.items():
    grp.attrs[key] = value

f.close()

# go to matlab [...]

# Markup is very suitable for metadata, maybe XML, JSON or even
# sophisticated formats for semantic metadata are already in use

# those formats follow similar paradigms and can be converted
# c.f. JSON and h5group for hydraulic fluid
json.dump(best_oil, open("./best_oil.json", "w", encoding="utf-8"),
          ensure_ascii=False, indent=4)

# Markup data can be used as is, with HDF5 as "carrier"!
# c.f.: read JSON content > store as CLOB
best_oil = json.load(open("./best_oil.json", encoding="utf-8"))
best_oil_json = json.dumps(best_oil, ensure_ascii=False, indent=4)

f = h5py.File("./example.h5", "r+")

dset = f.create_dataset("best_oil/json", data=best_oil_json)

f.close()

# interoperability & random access is invaluable! + uniform acccess interface
# to leverage it for Text data, split file contents or write line by line
# as Array! c.f. CLOB and Text Array in HDFView
f = h5py.File("./example.h5", "r+")

best_oil_json_lines = [line.rstrip('\n') for line in open("./best_oil.json", encoding="utf-8")]
best_oil_json_lines = np.array(best_oil_json_lines, dtype=h5py.special_dtype(vlen=str))

dset = f.create_dataset("best_oil/json_lines", data=best_oil_json_lines)

f.close()

# Redundancy can be avoided by using links and references
# c.f. multiple "URLs" pointing to the same Element, even across files
f = h5py.File("./example.h5", "r+")

f["components/fluid"] = f["best_oil"]

f.close()
